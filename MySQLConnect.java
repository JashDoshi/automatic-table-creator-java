import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import java.util.HashMap;
public class MySQLConnect {
    
    Connection conn;
    
    
    public static Connection connectDB(){
        HashMap<String,String> hashMap;
        ReadFile fileReader=new ReadFile("config.ini");
        String fileContent = fileReader.readFile();
        hashMap = simplify(fileContent);
        try{
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/"+hashMap.get("database_name"), hashMap.get("username"), hashMap.get("password"));
            return conn;
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, "Connection Failed! " + e);
            return null;
        }
    }
    static HashMap simplify(String str){
        HashMap<String,String> hm=new HashMap();
        String arr[] =str.split(",");
        for(int i=0;i<arr.length;i++){
            String keyVal[]=arr[i].split(":");
            String key=keyVal[0].trim();
            String val=keyVal[1].trim();
            hm.put(key,val);
        }
        return hm;
    }
}
