# Automatic Table Creator

Format for JSON file
{
	"table" : {
		"table_name" : "any_name",
		"columns" : {
			"attr1" : "col_name",
			"attr2" : "col_name",
			...
			"datatype" : {
				"col_name" : "datatype",
				....
				"constraint" : {
					"primary_key" : "col_name"
				}
			}
		}
	}
}
Make a new JSON file for every table you want to create.
Keep that file in database/migrations folder.