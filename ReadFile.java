import java.io.File;
import java.io.FileInputStream;
class ReadFile{
	String fileName;
	File inFile;
	ReadFile(String fileName){
		this.fileName=fileName;
		
	}
	String readFile(){
		try
		{
			inFile=new File(fileName);
			String str="";
			char ch;
			int b;
			FileInputStream in=new FileInputStream(inFile);
			while((b=in.read())!=-1)
			{
				ch=(char)b;
				str += ch;
			}
			str=str.replaceAll("\\s+"," ");
			// System.out.println(str+"\n\n");
			in.close();
			return str;
		}
		catch (Exception e){}
		return "";
	}
}