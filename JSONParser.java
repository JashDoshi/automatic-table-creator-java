import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;
class JSONParser{
	HashMap<String,Object> mainHash=new HashMap();
	// String fileName="sample2.json";
	// File inFile;
	// String str="";
	int i=0,j=0;
	int top=-1;
	int hashTop=-1;
	int keyTop=-1;
	int[] arr=new int[100];
	HashMap[] hashArr=new HashMap[100];
	String[] keyArr=new String[100];
	// JSONParser(){
	// 	inFile=new File(fileName);
	// 	str=readJSON(inFile);
	// 	mainHash=parseJSON(str);
	// 	System.out.println(mainHash);
		// HashMap m=(HashMap)((HashMap)mainHash.get("glossary")).get("GlossDiv");
		// HashMap m1=(HashMap)m.get("GlossList");
		// HashMap m2=(HashMap)m1.get("GlossEntry");
		// System.out.println(m2.get("GlossTerm"));
		// System.out.println(str);
	// }
	String readJSON(File inFile){
		try
		{
			String str="";
			char ch;
			int b;
			FileInputStream in=new FileInputStream(inFile);
			while((b=in.read())!=-1)
			{
				ch=(char)b;
				str += ch;
			}
			str=str.replaceAll("\\s+"," ");
			// System.out.println(str+"\n\n");
			in.close();
			return str;
		}
		catch (Exception e){}
		return "";
	}
	void push(int no){
		arr[++top]=no;
	}
	int pop(){
		return arr[top--];
	}
	void pushHash(HashMap hm){
		hashArr[++hashTop]=hm;
	}
	HashMap popHash(int no){
		return hashArr[no];
	}
	void pushKey(String key){
		keyArr[++keyTop]=key;
	}
	String popKey(int no){
		return keyArr[no];
	}
	HashMap parseJSON(String str){
		HashMap hashMap;
		HashMap<String,Object> hash;
		while(i!=str.length()){
			
			if(str.charAt(i)=='{')
				push(i);
			else if(str.charAt(i)=='}'){
				int start=pop();
				String sub=str.substring(start+1,i);
				hashMap=simplify(sub);
				if(start>0 && start<str.length()){
					hash=new HashMap();
					while(str.charAt(start)!='"')
						start--;
					int last=start;
					start--;
					while(str.charAt(start)!='"')
						start--;
					// System.out.println(str.substring(start+1,last));
					pushKey(str.substring(start+1,last));
					pushHash(hashMap);
					int end=i+2;
					i=i-str.substring(start,end).length();
					str=str.replace(str.substring(start,end)," ");
				}
				
				// System.out.println(str);
				// System.out.println("\n\n");
			}
			i++;
		}
		HashMap<String,Object> main=new HashMap();
		int j=0;
		for(int i=hashTop;i>=0;i--){
			if(j==0){
				main.put(popKey(i),popHash(i));
				j++;i++;
			}
			else if(i!=0)
				popHash(i).put(popKey(i-1),popHash(i-1));
			// System.out.println("\n"+popHash(i)+"\n");
		}
		return main;
	}
	HashMap simplify(String str){
		HashMap<String,String> hashMap=new HashMap();
		String[] arr=str.split(",");
		for(int i=0;i<arr.length;i++){
			if(!((arr[i].trim()).equals(""))){
				String[] keyVal=arr[i].split(":");
				String key=keyVal[0].replaceAll("\""," ");
				String val=keyVal[1].replaceAll("\""," ");
				key=key.trim();
				val=val.trim();
				hashMap.put(key,val);
				// System.out.println(key + " : " + val);
			}
		}
		return hashMap;
	}
	public static void main(String[] args) {
		new JSONParser();
	}
}