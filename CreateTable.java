import java.util.HashMap;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
class CreateTable{
	ReadFile fileReader;
    String fileContent;
    HashMap<String,Object> hashMap;
    Connection conn;
    CreateTable(){
    	fileReader=new ReadFile("database\\migrations\\table1.json");
    	fileContent = fileReader.readFile();
    	JSONParser jsonParser=new JSONParser();
    	hashMap = jsonParser.parseJSON(fileContent);
 		createConn(hashMap);
 		createTable();
    }
    void createConn(HashMap hashMap){
    	if(hashMap!=null){
    		conn=MySQLConnect.connectDB();
    		System.out.println(conn);
    	}
    }
    void createTable(){
    	PreparedStatement ps ;
    	String tableName=(String)((HashMap)hashMap.get("table")).get("table_name");
    	HashMap colName = (HashMap)((HashMap)hashMap.get("table")).get("columns");
    	HashMap datatype = (HashMap)colName.get("datatype");
   		int colNo=colName.size();
   		String sql="CREATE TABLE "+tableName+" (";
   		String alter = "ALTER TABLE "+tableName+" ADD ";
   		for(int i=1;i<colNo;i++){
   			if(i==colNo-1)
   				sql=sql+colName.get("attr"+i)+" "+datatype.get((String)colName.get("attr"+i));
   			else
   				sql=sql+colName.get("attr"+i)+" "+datatype.get((String)colName.get("attr"+i))+",";
   		}
   		sql = sql + ");";
    	System.out.println(sql);
    	try{
	    	ps =conn.prepareStatement(sql);
	        ps.execute();
	        
	    }catch(Exception e){
	    	System.out.println(e);
	    }
	    HashMap constraint= (HashMap)datatype.get("constraint");
   		if(constraint!=null){
   			if((String)constraint.get("primary_key")!=null){
   				alter = alter+"PRIMARY KEY ("+constraint.get("primary_key")+");";
   				System.out.println(alter);
   				try{
			    	ps =conn.prepareStatement(alter);
			        ps.execute();   
			    }catch(Exception e){
			    	System.out.println(e);
			    }
   			}
   		}
    }
    public static void main(String[] args) {
    	new CreateTable();
    }

}